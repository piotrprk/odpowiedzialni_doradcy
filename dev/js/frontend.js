(function($){
$(document).ready(function() {
    
// Fullscreen menu
	$('.navbar-toggle').on('click', function(e){

		$(document).keyup(function(e){
		    if(e.keyCode === 27)
		        $('.fullscreen-menu-wrapper').fadeOut();
		});

		$('.fullscreen-menu-wrapper').fadeIn();		
	});

	$('.menu-close-btn').on('click', function(e){
		$('.fullscreen-menu-wrapper').fadeOut();
	});

//detect touch device

var mobileHover = function () {
    $('a').on('touchstart', function () {
        $(this).trigger('hover');
    }).on('touchend', function () {
        $(this).trigger('hover');
    });
};
mobileHover();
    
//home page search icons change on click
$('.search-form-one .input-group').on('shown.bs.dropdown', function () {

        $('a.btn>input~span', this)
            .toggleClass("chevron-down", 0)
            .toggleClass("chevron-up", 1);

        //focus on input 
        $('input', this).focus();
});
$('.search-form-one .input-group').on('hidden.bs.dropdown', function () {

        $('a.btn>input~span', this)
            .toggleClass("chevron-down", 1)
            .toggleClass("chevron-up", 0);

        //focus on input 
        $('input', this).focus();
});    
// home page dropdown selector value copy to input field
$('.search-form-one .input-group').find('.dropdown-menu li a').click( function(){
    $('input', $(this).closest('.input-group')).val($(this).text());
});
    
//live search on dropdown list
$('.search-form-one .input-group .dropdown-menu li a').each(function(){
   $(this).attr('data-search-term', $(this).text().toLowerCase());
});
    
var livesearch = function(thisObj){
    //keep the dropdown opened
    if ($(thisObj.closest('.dropdown-toggle')).attr('aria-expanded') === 'false'){
        $(thisObj.closest('.dropdown-toggle')).dropdown('toggle');
    }
    var searchTerm = $(thisObj).val().toLowerCase();
    $('.dropdown-menu li a', thisObj.closest('.input-group')).each(function(){
        if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
    $(thisObj).focus();    
}

$('#doradca-search').on('keyup', function(){
    livesearch(this);
});
    
$('#miasto-search').on('keyup', function(){
    livesearch(this);
});  

//show popup on link click
    
function dopopover() {
    var counter;
    $('[data-toggle="popover"]').popover({
        trigger: 'manual',
        html: true,
        content: function () {
            return $(this).parent().find('.description').html();            
        },
        container: '#top9-section',
        placement: 'auto top'
    }).on("click",function () {
        var _this = this; // popover container
        // zero the counter
        clearTimeout(counter);
        // Close all other Popovers
        $('[data-toggle="popover"]').not(_this).popover('hide');
        $(_this).popover("show");
        // start new timeout to show popover
        counter = setTimeout(function(){
                $(_this).popover("show");
            }, 200);
    });
    //close all popovers on click, usable on touchscreens
    $('body').click(function () {
       $('[data-toggle="popover"]').popover('hide'); 
    });
}
dopopover();

 

//star rating
// take rating score (rating-value) from an input being a child of .star-rating class div 
// display rating using Font Awesome classes .circle and diffent colors (class .color-gray)
displayrating = function(){
    $('.star-rating').each(function(){
        var rating = parseInt($(this).find('input.rating-value').val());
        setrating($(this), rating);
    });
};
setrating = function(object, rating){
    object.find('input.rating-value').val(rating+"*");
    object.find('.fa').each(function(){
        if (rating >= parseInt($(this).data('rating'))){
                return $(this).removeClass('color-gray');
            } else {
                return $(this).addClass('color-gray');
            }        
        });      
};
    
displayrating();

//rate setting behavior
//set rating on click
$('.add-a-comment .star-rating').on("mouseover", function(){
            $(this).find('.fa').on("mouseover", function(){
                var rating = $(this).data('rating');
                if ($(this).parent().data('is-set') != 1){ 
                    setrating($(this).parent(), rating);            
                }
            });              
    }).on("mouseleave", function(){    
        if ($(this).data('is-set') != 1){
            setrating($(this).parent(), 0);        
        }
    }).on("click", function(){
        if ($(this).data('is-set') != 1){
            $(this).data('is-set', "1");
        } 
        $(this).find('.fa').on("click", function(){
            setrating($(this).parent(), $(this).data('rating'));
        });        
//        console.log($(this).data('is-set'));
});

//opinions-bar horizontal bar initialize
//numbers are stored in the data-number param
myfloor = function(number, precision) {
// round down number with given precision
    var factor = Math.pow(10, precision);
    var tempNumber = number * factor;
    var roundedTempNumber = Math.floor(tempNumber);
    return roundedTempNumber / factor;
};
    
$('.opinions-bar .bar').each(function(){
    var positive = parseInt($(this).find('.positive').data('number'));
    var negative = parseInt($(this).find('.negative').data('number'));
    var positive_percent;
    $(this).children('.positive').css('width', function(){
        positive_percent = (positive/(positive+negative))*100;
        positive_percent = myfloor(positive_percent, -1);
        return positive_percent  + '%';
    });
    $(this).children('.negative').css('width', function(){
        return (100 - positive_percent) + '%';
    });
});


    
});
})(jQuery);

//submit form by ID, usable when more than one form on page
//
function submitFormbyID(id) { document.getElementById(id).submit();}