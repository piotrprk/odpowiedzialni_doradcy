var gulp = require('gulp'),
	del = require('del'),
	concat = require('gulp-concat'),
	less = require('gulp-less'),
	rename = require('gulp-rename'),
    strip = require('gulp-strip-comments'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    minify = require('gulp-minify-css'),
    uglify = require('gulp-uglify'), 
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect'),
    path = require('path'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    watch = require('gulp-watch'),
    htmlhint = require("gulp-htmlhint"),
    plumber = require('gulp-plumber');

var paths = {
    assets: './bower_components',
    destDir: './public',
    devDir: './dev'
};

// --- tasks

gulp.task('clean', function(cb) {
  del(paths.destDir, cb);
});

gulp.task('copyfonts', function() {
    gulp.src(paths.assets + '/bootstrap/fonts/**.*')
        .pipe(gulp.dest(paths.destDir + '/fonts'));    
    gulp.src(paths.assets + '/font-awesome/fonts/**.*')
        .pipe(gulp.dest(paths.destDir + '/fonts'));
    gulp.src(paths.assets + '/font-awesome/css/font-awesome.min.css')
        .pipe(gulp.dest(paths.destDir + '/css'));   
    gulp.src(paths.devDir + '/fonts/**/*')
        .pipe(gulp.dest(paths.destDir + '/fonts'));    
});
gulp.task('copyimages', function() {
    gulp.src(paths.devDir + '/img/*.svg')
        .pipe(gulp.dest(paths.destDir + '/img'))
        .pipe(livereload());    
    return gulp.src(paths.devDir + '/img/**/*.{png,gif,jpg,jpeg}')
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths.destDir + '/img'))
        .pipe(livereload());
    
});
gulp.task('copyhtml', function() {
    return gulp.src(paths.devDir + '/*.{htm,html,php}')
        .pipe(htmlhint())
        .pipe(htmlhint.reporter())
        .pipe(gulp.dest(paths.destDir))
        .pipe(livereload());    
});

// --- compile less files and minify
gulp.task('less', function () {
    var processors = [
        autoprefixer({browsers: ['last 2 versions']}),
    ];    
  return gulp.src(paths.devDir + '/less/frontend.less')
    .pipe(plumber({
        handleError: function (err) {
            console.log(err);
            this.emit('end');
        }
    }))
    .pipe(less())
    .on('error', function(err){
        console.log(err);
        this.emit('end');
    })
    .pipe(postcss(processors))
    .pipe(gulp.dest(paths.destDir + '/css'))
    .pipe(livereload())
    .pipe(minify({keepSpecialComments:0}))
    .pipe(rename({suffix: '.min'}))
    .pipe(plumber.stop())
    .pipe(gulp.dest(paths.destDir + '/css'));    
});

// --- JS frontend
gulp.task('js', function(){  
  return gulp.src([
//      paths.assets +'/jquery/dist/jquery.js', //get jquery min in the html file
      paths.assets +'/bootstrap/dist/js/bootstrap.js',
//      paths.assets +'/jquery-ui/ui/jquery-ui.js', //jqueryUI all      
//      paths.assets +'/jquery-ui/ui/core.js', //jqueryUI core
//      paths.assets +'/jquery-ui/ui/widget.js', //jqueryUI widget component
//      paths.assets +'/jquery-ui/ui/widgets/slider.js', //jqueryUI widget-slider
//      paths.assets +'/jquery-ui/ui/widgets/mouse.js', //jqueryUI aditional component
      paths.devDir +'/js/frontend.js'
    ])
    .pipe(plumber({
        handleError: function (err) {
            console.log(err);
            this.emit('end');
        }
    }))
    .pipe(concat('frontend.min.js'))
    .pipe(uglify())
    .pipe(plumber.stop())
    .pipe(gulp.dest(paths.destDir + '/js'));
});
// --- watch
gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});
gulp.task('watch', function() {
    connect.server({
        livereload: true
    });    
    livereload.listen();
    gulp.watch(paths.devDir + '/less/*.less', ['less']);
    gulp.watch(paths.devDir + '/js/frontend.js', ['js']);
//    gulp.watch(paths.devDir + '/img/*', ['copyimages']);
    gulp.watch(paths.devDir + '/*.{htm,html,php}', ['copyhtml']);    

    watch(paths.devDir + '/img/**/*.{png,gif,jpg,jpeg,svg}', { ignoreInitial: false })
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths.destDir + '/img'))
        .pipe(livereload());
    watch(paths.devDir + '/fonts/*', { ignoreInitial: false })        
        .pipe(gulp.dest(paths.destDir + '/fonts'))
        .pipe(livereload());    
});

// ---default task to run it all
    gulp.task('default', ['copyfonts', 'copyimages', 'copyhtml', 'less', 'js']);